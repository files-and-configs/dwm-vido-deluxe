1. SESIÓN

	- SUPER + F1			Mostrar esta ayuda
	- SUPER + SHIFT + Q		Cierre de sesión / apagado
	- SUPER + SHIFT + R		Reiniciar dwm
	- SUPER + ESC		        Bloquear pantalla


2. PROGRAMAS

	- SUPER + D			Menú de ejecución de programas (dmenu)
	- SUPER + ENTER			Terminal (st)
	- SUPER + B			Navegador de internet (firefox)
	- SUPER + SHIFT + B		Navegador de internet alternativo (qutebrowser)
	- SUPER + W			Gestor de archivos (emacs + dired)
	- SUPER + SHIFT + W		Gestor de archivos alternativo (pcmanfm)
	- SUPER + E			Editor de texto (emacs)


3. DESPLAZAR FOCO

	- SUPER + J / SUPER + ↓		Enfocar ventana siguiente
	- SUPER + K / SUPER + ↑		Enfocar ventana anterior
	- SUPER + H / SUPER + ←		Entrechar ventana maestra
	- SUPER + L / SUPER + →		Ensanchar ventana maestra


4. GESTIONAR VENTANAS

	- SUPER + C			Cerrar ventana actual
	- SUPER + SPC			Enfocar ventana maestra
	- SUPER + CTRL + B		(Des)Activar barra superior
	- SUPER + I			Incrementar el número de ventanas maestras
	- SUPER + SHIFT + I		Decrementar el número de ventanas maestras
	- SUPER + F			Ventana actual a pantalla completa
	- SUPER + G			(Des)Activar huecos entre ventanas
	- SUPER + T			(Des)Hacer ventana actual flotante
	- SUPER + F3			(Des)Hacer todas las ventanas flotantes
	- SUPER + F2			Activar modo Maetro y pila
	- SUPER + F4			Activar modo Monóculo
	- SUPER + SHIFT + TAB		Cambiar a modo anterior


5. GESTIONAR ETIQUETAS

	- SUPER + 1..9			Ir a la etiqueta 1..9
	- SUPER + SHIFT + 1..9		Cambiar etiqueta de ventana actual a etiqueta 1..9
	- SUPER + CTRL + 1..9		Mostrar etiqueta 1..9
	- SUPER + 0			Mostrar todas las etiquetas
	- SUPER + SHIFT + 0		Colocar todas las etiquetas a la ventana actual
	- SUPER + TAB			Ver etiqueta anterior


6. SCRIPTS

	- SUPER + M			Selección de música (mpd + mpc)
	- SUPER + P			Selección de contraseñas (pass)
	- SUPER + O			Dispositivos de salida de audio
	- SUPER + X			Detener procesos
	- SUPER + INSERT		Insertar snippet
	- SUPER + PRNT. SCR		Captura de pantalla completa (maim)
	- SUPER + PLUS			Subir volumen de mpd (mpc)
	- SUPER + MINUS			Bajar volumen de mpd (mpc)
	- SUPER + S			Selección de scripts


7. RATÓN

	- SUPER + LEFT CLICK (DRAG)	Mover ventana flotante
	- SUPER + RIGHT CLICK (DRAG)	Cambiar tamaño de ventana flotante
